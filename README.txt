$Id



This is the BannerConnect Adspace module. 
You can use this module if you want to add premium advertising ads on your Drupal web site.



Installation
------------

Copy the bannerconnect_adspace to your module directory and then enable on the admin
modules page.  

In the settings, which can be found at admin/settings/bannerconnect_adspace, 
you can select the different sizes of ads you want to have on your website.

You can also type your section ID here, so you will be paid for the ads.

Once you setup the settings for the BannerConnect adspace module, you can enable the ads under admin/build/block. 
For each ad a block is created which you can move to the place you want. 


Signup at BannerConnect for a section ID so you can get paid for the ads. 

Author
------
Niki Mathijsen
niki@techconnect.nl
