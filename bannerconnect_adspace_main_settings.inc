<?php

function bannerconnect_adspace_main_settings() {
  $form =  array();
  $form['help'] = array(
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#title'       => t('Help and instructions'),
  );
  $form['help']['help'] = array(
    '#type'  => 'markup',
    '#value' => bannerconnect_adspace_get_help_text(),
  );

  $form['configuration'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Configuration'),
  );
  $bannerconnect_adspace = variable_get('bannerconnect_adspaces', '');
  $bannerconnect_adspaces =  array();
  foreach (explode(",", $bannerconnect_adspace) as $bannerconnect_piece) {
    if (!empty($bannerconnect_piece)) {
      $bannerconnect_adspaces[] = trim($bannerconnect_piece);
    }
  }

  $bannerconnect_section_id = variable_get('bannerconnect_adspace_section_id', '');
  $form['configuration']['numbers'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Sizes'),
    '#options' => array('120 X 600' => '120 X 600', '160 X 600' =>  '160 X 600', '300 X 250' => '300 X 250', '336 X 280' => '336 X 280' , '468 X 60' => '468 X 60' , '728 X 90' => '728 X 90'),
    '#default_value' => $bannerconnect_adspaces,
    '#description' => t('Please choose size you want to have displayed on your site.')
  );
  $form['configuration']['secid'] = array(
    '#type' => 'textfield',
    '#title' => t('BannerConnect Section ID'),
    '#default_value' => $bannerconnect_section_id,
    '#description' => t('Please insert your BannerConnect section id.')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration'
  );
  return $form;
}

function bannerconnect_adspace_main_settings_validate($form, &$form_state) {
  $bannerconnect_section_id =  $form_state['values']['secid'];
  if (drupal_strlen($bannerconnect_section_id) > 0  && !is_numeric($bannerconnect_section_id)) {
    form_set_error('secid', t('Please enter a correct numeric BannerConnect section Id.'));
  }
}
function bannerconnect_adspace_main_settings_submit($form, &$form_state) {
  $bannerconnect_numbers = implode(', ', $form_state['values']['numbers']);
  $bannerconnect_section_id = check_plain($form_state['values']['secid']);
  variable_set('bannerconnect_adspaces', $bannerconnect_numbers);
  variable_set('bannerconnect_adspace_section_id', $bannerconnect_section_id);
}

function bannerconnect_adspace_get_help_text() {
$bannerconnect_output = '
<h2>Overview</h2>
<p>This module provides a simple way of displaying premium BannerConnect ads on your drupal site. </p>

<h2>Prerequisites</h2>
<p>You must have a BannerConnect publisher account before using this module.</p>
<p> If you do not have a BannerConnect publisher account <a href="http://www.bannerconnect.net/publishers/signup"> Click here</a> 

<h3> Note </h3>
<p> If you do not enter a  BannerConnect section ID, you will get demo ads which are not being paid </p>

<h2>Configuration</h2>
<p>To use this module, simply enter your Bannerconnect section ID in the text field, select a size you want to have displayed on your site and click
the \'Save configuration\' button.</p>
<p> You can change the style of the DIV that surrounds an ad by making changes in the CSS file located at root/sites/all/modules/bannerconnect_adspace.css</p>

<h2> Sizes </h2>
<table>
<thead>
<tr> 
<th>Format</th>
<th>Width</th>
<th>Height</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>120 X 600</td>
<td>120</td>
<td>600</td>
</tr>
<tr class="even">
<td>160 X 600</td>
<td>160</td>
<td>600</td>
</tr>

<tr class="odd">
<td>300 X 250</td>
<td>300</td>
<td>250</td>
</tr>
<tr class="even">
<td>336 X 280</td>
<td>336</td>
<td>280</td>
</tr>

<tr class="odd">
<td>468 X 60</td>
<td>468</td>
<td>60</td>
</tr>
<tr class="even">
<td>728 X 90</td>
<td>728</td>
<td>90</td>
</tr>
</tbody>
</table>
';
return $bannerconnect_output;
}
